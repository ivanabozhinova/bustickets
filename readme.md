# Безбедност на компјутерски системи #
# Апликација за купување автобуски билети #
 
## Вовед ##

Целта на овој проект е изработка на апликација која што ќе запазува дел од широко прифатените стандарди за обезбедување сигурност на WEB апликации. За демонстрација на имплеметираната сигурност развивме повеќенивовски систем кој ги автентицра и авторизира различните типови на корисници и ги ограничува нивните привилегии. Исто така, симулирано е и сигурно електронско плаќање на билети. 

Апликацијата е хостирана на следната адреса https://192.157.220.142, а изворниот код е достапен на https://bitbucket.org/ibozinova/bustickets.

## Користени технологии при развој на веб апликацијата
Апликацијата е изработена во програмскиот јазик PHP, користејќи го добродокументираниот framework CodeIgniter за архитектурата и бизнис логиката на апликацијата. Работена е според МVC шаблонот за развој на софтвер. За изгледот на е користен Bootstrap framework-от.

## Стандарди за WEB безбедност

### 1.  Валидирање на влез
**Заштита од XSS напад** 

За превенција на овој напад се користи xss_clean функцијата која ја нуди CodeIgniter.  Таа е сетирана автоматски да ги филтрира сите POST  и COOKIE податоци на глобално апликациско ниво. Оваа функција спречува извршување на скрипти и друг тип на малициозен код, така што бара често користени техники за напади. Доколку се внесе недозволен код тој е конвертиран во character entities. 

**Заштита од  Local File Inclusion**

Апликацијата е заштитена од ваков тип на напад бидејќи се проверува делот од url-то кој ќе ја одреди следната страна што треба да се прикаже. Дозволени карактери се само буквите од англиската азбука, како и „_“ и „-“, но не и карактерот „/“ кој претставува најголема опасност.

**Заштита од SQL Injection** 

CodeIgniter користи модифицирана верзија на шаблонот Аctive Record за работа со база на податоци. Бидејќи вредностите се заштитени (escaped) автоматски од системот,  со правилно користење на Active Record добивме безбедни SQL барања.

**Заштита од Cross-site request forgery (CSRF)**

Ова е напад со неавторизирани команди од корисник на кој ситемот му верува. Може да се изведе ако некоја форма на страната на напаѓачот покажува кон нашата страна. Овој напад се спречува со вметнување на рандом стриг (токен) во секоја форма и негово зачувување во корисничката сесија. На секое POST  барање, го споредуваме поднесениот токен со зачуваниот и ако тие се разликуваат, тогаш барањето се одбива. Ова е постигнато со тоа што вредноста на глобалната променлива $config['csrf_protection'] е сетирана на TRUE, па во секоја форма автоматски се поставува csrf поле. 

### 2. Санитација на излез
CodeIgniter овозможува повеќе околини на работа со апликацијата. Во фазата на развој, апликацијата беше во development околина, во која сите грешки се видливи во пребарувачот. Откако завршивме со развивањето на апликацијата ја поставивме во продукциски мод, во кој е исклучено пријавувањето на PHP грешки во апликацијата. Со ова е спречено грешките да се прикажуваат на излез бидејќи тие можат да соджат чувствителни информации кои може да бидат искористени од напаѓачот.  

Корисникот добива предефинирани пораки за грешка при:

•	Регистрација, ако внесената e-mail адреса веќе постои во базата на податоци.

•	Внесување на погрешно корисничко име или лозинка при логирање, тогаш добива порака дека внел погрешни податоци. 

•	Обид за пристап до некоја од страниците кои бараат претходна автентикација, без да се најави

•	Обид да пристапи до страниците кои бараат сертификат кој тој го нема

Нема делови во апликацијата во кои што на излез даваме кориснички дефинирани податоци, па затоа не се јави потреба истиот да се санитира.

### 3. Сепарација на корисници и должности
Во апликацијата се имплементирани три нивоа на корисници и тоа :
1.	административно ниво
2.	ниво на вработени
3.	корисничко ниво

*1.	Административно ниво*
Отако успешно ќе се автентицира и авторизира администраторот, тој добива мени од кое му е овозможен пристап до следните страници :
* •	Компании
* •	Дестинации
* •	Линии
* •	Корисници
* •	Типови на корисници
* •	Автобуси
Од секоја од овие страници, тој може да додава, брише или менува податоци од соодветната табела во базата на податоци.

*2.	Ниво на вработен*
Слично како и за администраторот, по успешна автентикација и авторизација, вработеното лице добива пристап до страница на која има листа од сите резервирани билети. Кога корисникот доаѓа на шалтер во автобуската станица и го купува резервираниот билет,  вработениот може да означи дека се случило соодветното плаќање.

*3.	Корисничко ниво*
Корисникот без најава може да пристапи  до следните страници:
* •	Возен ред
* •	Почетна страна
* •	Контакт
* •	Најава
* •	Регистација
Ако сака да да резервира или купи карта, или пак само да си го прегледа својот профил, тогаш ќе мора да се најави. Откако се успешно се регистрирал  и најавил може да резервира билети и  доколку има соодветен сертификат, да ги плати. Секој корисник може да го прегледа само сопствениот профил, каде може да најде листа од сите резервирани и купени билети.

### 4.  Креирање на корисници со минимални привилегии
Обичен корисник без инсталиран сертификат на својот пребарувач може да се регистрира, потоа да се логира, да си одбира и резервира билети, но не може да го реализира онлајн плаќањето.

### 5. Автентикација на корисници

**Чување на лозинките во база на податоци**

После извесни истражувања за безбедносните мерки кои се превземаат во денешните веб апликации, дојдовме до заклучок дека *MD5, SHA1, SHA256, SHA512, SHA-3* и сличните алгоритми за хеширање не се добри кора станува збор за лозинки. Тие се наменети за генерални цели, а за оваа намена се сосема небезбедни. Модерен сервер може да пресмета MD5 хеш со брзина од околу 330МВ во секунда, што значи дека ако корисниците имаат пасворди составени од 6 алфанумерички карактери, секоја можна комбинација може да се испроба за само 40 секунди.

Следно, ја истражувавме безбедноста на* Salt* – рандом податоци користени како дополнителен влез во функција за хеширање, но дојдовме до заклучок дека ниту овој метод не ги заштитува лозинките од brute-force напад.

Затоа одлучивме да го користиме *Bycript* алгоритмот за хеширање на лозинки. Тој користи варијанта на Blowfish алгоритмот за енкрипција и има работен фактор кој ја одредува цената на хеш функцијата – во колку циклуси ќе се одвива хеширањето. Фазата на енкрипција е иста како кај Blowfish, но фазата на распоредување на клучевите обезбедува секоја следна состојба да зависи од salt и клучот (лозинката на корисникот) и ниту една состојба не може да се пресмета без да се знаат и двете вредности. Поради ваквата разлика во клучевите алгоритмот е еднонасочен.
Овој алгоритам работи многу бавно, зависно од работниот фактор. Ако тој фактор е поставен на 12, секоја можна лозинка може да се испроба за минимум 14 години. Тоа што е бавен и се одвива во повеќе циклуси обезбедува сигурност дека на напаѓачот ќе му биде потребно нереално долго време и средства за да ги пробие лозинките. Додатното користење на salts речиси целосно ги оневозможува нападите.

### 6. Авторизација на корисници
**X.509 сертификати**

Авторизацијата се обезбедува со помош на SSL протоколот така што :
* •	едно авторизирано тело (gashteovski.andrej@finki.ukim.mk) има овластување да издава сертификати на  останати ентитети во зависност од нивните привилегии.
* •	Останатите тела (bozinova.ivana@finki.ukim.mk и dichevska.snezana@finki.ukim.mk и серверот) инсталирајќи ги своите приватни клучеви доделени од авторизираното тело во прелистувачот, ги испраќаат своите сертификати до серверот и обратно за взаемна автентикација.
* •	 Доколку постапката заврши успешно, на корисниците им е овозможено соодветно ниво  на видливост односно пристап до апликацијата. 

**Apache**
Apache е конфигуриран така што не побарува сертификати за пристап до секоја страница, но ако во прелистувачот имаме инсталирано сертификат истиот го вчитува. 
 
На портата 80 каде што се одвива мрежниот сообраќај преку  HTTP протоколот целосно е забранет  пристапот до апликацијата, што значи дека комуникацијата со серверот може да се одвива само преку HTTPS простоколот користејќи ги соодветните сертификати, односно на порта 443 .

**PHP**
Бидејќи користиме MVC шаблон за развој на софтвер, фајловите кои се прикажуваат не се пристапуваат директно, туку контролерот ги вчитува како класи. Со конфигурирање на Apache добиваме ограничени привилегии само при директен пристап на фајловите, па проверката на сертификатите ја правиме на ниво на PHP, при што ја користиме истата логика од Apache, преку $_SERVER променливата, од која ги проверуваме вредностите на SSL_CLIENT_I_DN_CN и SSL_CLIENT_S_DN_CN.

*Администратор*
Сите страници и функции од ова ниво се пристапуваат преку контролерот admin во чиј конструктор се проверува Common Name на субјектот на кого е издаден сертификатот,  односно $_SERVER['SSL_CLIENT_S_DN_CN']. Ако оваа вредност е било што различно од  gashteovski.andrej@finki.ukim.mk,   кој  е единствениот корисник во нашиот систем со администраторски привилегии, тоа значи дека потребниот сертификат недостасува,  па се забранува пристап до соодветните страници. 
 
*Вработен*
Страниците од ова ниво се пристапуваат преку контролерот employee. Во неговиот конструктор проверуваме дали $_SERVER['SSL_CLIENT_S_DN_CN'] одговара на gashteovski.andrej@finki.ukim.mk или на ibozinova, корисниците кои имаат привилегии на вработен. Aко е така, авторизацијата е успешна и корисникот  ги добива соодветните привилегии, а во спротивно му се забранува пристапот.
 
*Корисник  и онлајн плаќање*
Корисникот нема потреба од додатна автентикација и авторизација со сертификати, се’ додека не пристапи до делот за онлајн плаќање на билетот. Затоа, во pages контолерот, од кои навигира до сите страници освен онаа за плаќање нема никакви ограничувања. Со клик на копчето „Купи“ тој е пренасочен до контролерот payment, за чиј пристап мора да има сертификат издаден од gashteovski.andrej@finki.ukim.mk. или да биде самиот администратор. Затоа проверуваме дали $_SERVER['SSL_CLIENT_I_DN_CN'] или $_SERVER['SSL_CLIENT_S_DN_CN'] одговараат на таа e-mail адреса.
 
 
 
## Проблеми во обезбедување на сигурност
 
### Имплементирање на сертификати на сервер со една порта за https
Најпрвин користевме сервер на кои се хостирани повеќе виртуелни сервери користејќи иста IP адреса, со различни порти. За нашиот виртуелен сервер беше отворена само една порта за пристап преку HTTP и тој пристап функционираше добро. Се обидовме да конфигурираме HTTPS конекција преку таа порта, но прегледувачот воопшто не бараше сертификат за серверот, односно протоколот за автентикација не успеа ни да започне. На серверот најверојатно е овозможена HTTPS конекција преку порта 443, а поради тоа што немавме привилегии до главната конфигурација, не можевме да видиме како се одвива пренасочувањето меѓу портите.
По долгите обиди и истражувања, заклучивме дека е многу тешко да се овозможат повеќе порти за HTTPS конекција, а истото е невозможно без пристап до главните конфигурации на серверот, што беше причина за откажување од таа идеја.    

### Имплементирање на сертификати на бесплата виртуелна машина од Koding	
Втората идеја беше да се искористи бесплатен виртуелен сервер што го овозможува koding.com. Успеавме да добиеме виртуелен сервер кој го пристапувавме на поддоменот snivan.koding.kd.io. Откако ги направивме сите потребни конфигурации на серверот и ја поставивме апликацијата, пробавме да пристапиме со HTTPS кон серверот. Тогаш видовме дека на koding серверите веќе има воспоставено SSL конфигурација, и тоа користејќи wildcard сертификат за *.kd.io. Значи барањата проследени до сите сервери на некој поддомен на kd.io беа автентицирани користејќи го тој сертификат. Според тоа нашиот сертификат воопшто не беше искористен, така што пропадна и тој труд.

### Купување на VPS со повеќе ip адреси
Конечно успеавме да го конфигурираме серверот така што ќе ограничува и дозволува пристап соодветно на типот на сертификатот, но се појавија други проблеми. Земајќи ја во предвид природата на MVC шаблонот и имплементацијата на CodеIgniter дојдовме до заклучок дека не може да се ограничи пристап на контролерите од апликацијата преку серверска конфигурација и беше потребно истата да се доведе на апликациско ниво. Откако тој проблем беше решен апликацијата стана целосно функционална.