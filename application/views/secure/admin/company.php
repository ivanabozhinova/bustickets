<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#company');
    });
</script>
<!-- Activate the tab END-->

<h2>Компаниии</h2>
<?php if (isset($_POST['add'])) {
    $this->load->view('secure/admin/add/add-company');
} else {
    ?>
    <form action="<?php echo base_url()?>index.php/admin/view/company" method="post">
        <input type="submit" class="btn btn-success" name="add" value="Додади">
    </form>
<?php } ?>

<table class="table table-striped">
    <tr>
        <th>Име на компанија</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    $companies = $this->Company->get_all();
    foreach ($companies as $company) {
        ?>
        <tr>
            <td><?php echo $company['name'];?></td>
            <form action="<?php echo base_url();?>index.php/admin/view/admin-companies" method="post">
                <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
            </form>
            <form action="<?php echo base_url()."index.php/admin/delete/company/".$company['id'] ?>"method="post">
                <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
            </form>
        </tr>
    <?php } ?>
</table>
