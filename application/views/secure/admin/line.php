<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#line');
    });
</script>
<!-- Activate the tab END-->

<h2>Линии</h2>
<?php if (isset($_POST['add-new'])) {
    $this->load->view('secure/admin/add/add-line');
} else {
    ?>
    <form action="<?php echo base_url()?>index.php/admin/view/line" method="post">
        <input type="submit" class="btn btn-success" name="add-new" value="Додади">
    </form>
<?php } ?>



<table class="table table-striped tablesorter">
    <thead>
    <tr>
        <th><h4>Тргнува </h4></th>
        <th><h4>Пристига</h4></th>
        <th><h4>Време на поаѓање</h4></th>
        <th><h4>Цена<br></h4></th>
        <th><h4>Број на автобус</h4></th>
        <th><h4>Компанија</h4></th>
        <th></th>
        <th></th>

    </tr>
    </thead>
    <tbody>
    <?php
    $lines = $this->Line->get_table_data();
    foreach($lines as $line) {
        ?>
        <td><?php echo $line['start_destination_name'] ?></td>
        <td><?php echo $line['end_destination_name'] ?></td>
        <td><?php echo $line['start_time'] ?></td>
        <td><?php echo $line['price'] ?></td>
        <td><?php echo $line['bus_number'] ?></td>
        <td><?php echo $line['company_name'] ?></td>
        <form action="<?php echo base_url();?>index.php/admin/view/admin-companies" method="post">
            <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
        </form>
        <form action="<?php echo base_url()."index.php/admin/delete/line/".$line['id'] ?>"method="post">
            <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
        </form>

        </tr>
    <?php } ?>
    </tbody>
</table>