<div class="row-fluid">
    <hr class="prettyline">
    <form action="<?php echo base_url();?>index.php/admin/add/line" method="post">
        <fieldset>
            <!-- Text input-->
            <div class="control-group">

                <label class="control-label" for="start_destination_name">Почетна дестинација:</label>
                <div class="controls">
                    <!--   <input id="company_name" name="company_name" class="form-control" type="text" placeholder="Авто Атом" class="input-large" required=""> -->
                    <select id="start_destination_name" name="start_destination_name">
                        <?php
                        $destinations = $this->Destination->get_all();
                        foreach ($destinations as $destination) {
                            ?>
                            <option id="<?php echo $destination['id']?>"><?php echo $destination['name']?></option>

                        <?php } ?>
                    </select>
                </div>
                <label class="control-label" for="end_destination_name">Крајна дестинација:</label>
                <div class="controls">
                    <!--   <input id="company_name" name="company_name" class="form-control" type="text" placeholder="Авто Атом" class="input-large" required=""> -->
                    <select id="end_destination_name" name="end_destination_name">
                        <?php
                        $destinations = $this->Destination->get_all();
                        foreach ($destinations as $destination) {
                            ?>
                            <option id="<?php echo $destination['id']?>"><?php echo $destination['name']?></option>

                        <?php } ?>
                    </select>
                </div>

                <label class="control-label" for="start_time">Време на тргнување (да се валидира форматот):</label>
                <div class="controls">
                    <input id='start_time' name="start_time" class="form-control" type="time" placeholder="15" class="input-large" required="">
                </div>


                <label class="control-label" for="bus">Автобус:</label>
                <div class="controls">
                    <select id="bus" name="bus">
                        <?php
                        $buses = $this->Bus->get_all_data();
                        foreach ($buses as $bus) {
                            ?>
                            <option id="<?php echo $bus['id']?>"><?php echo $bus['print'] ?></option>
                        <?php } ?>
                    </select>
                </div>

                <label class="control-label" for="price">Цена:</label>
                <div class="controls">
                    <input id='price' name="price" class="form-control" type="number" placeholder="15" class="input-large" required="">
                </div>

            </div>
            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="add"></label>
                <div class="controls">
                    <input type="submit" id="add" name="add" value="Додади" class="btn btn-success"/>
                </div>
            </div>
        </fieldset>
    </form>
    <hr class="prettyline">
</div>
