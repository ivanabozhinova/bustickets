<div class="row-fluid">
    <hr class="prettyline">
    <form action="<?php echo base_url();?>index.php/admin/add/destination" method="post">
        <fieldset>
            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="name">Дестинација:</label>
                <div class="controls">
                    <input id="name" name="name" class="form-control" type="text" class="input-large" required="">
                </div>
            </div>
            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="add"></label>
                <div class="controls">
                    <input type="submit" id="add" name="add" value="Додади" class="btn btn-success"/>
                </div>
            </div>
        </fieldset>
    </form>
    <hr class="prettyline">
</div>
