<div class="row-fluid">
    <hr class="prettyline">
    <form action="<?php echo base_url();?>index.php/admin/add/bus" method="post">
        <fieldset>
            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="'bus_number">Број на автобус:</label>
                <div class="controls">
                    <input id='bus_number' name="bus_number" class="form-control" type="text" placeholder="15" class="input-large" required="">
                </div>
                <label class="control-label" for="company_name">Компанија:</label>
                <div class="controls">
                 <!--   <input id="company_name" name="company_name" class="form-control" type="text" placeholder="Авто Атом" class="input-large" required=""> -->
                    <select id="company_name" name="company_name">
                        <?php
                            $companies = $this->Company->get_all();
                            foreach ($companies as $company) {
                        ?>
                            <option id="<?php echo $company['id']?>"><?php echo $company['name']?></option>

                        <?php } ?>
                    </select>
                </div>
            </div>
            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="add"></label>
                <div class="controls">
                    <input type="submit" id="add" name="add" value="Додади" class="btn btn-success"/>
                </div>
            </div>
        </fieldset>
    </form>
    <hr class="prettyline">
</div>
