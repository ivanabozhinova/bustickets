<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#destination');
    });
</script>
<!-- Activate the tab END-->


<h2>Дестинации</h2>
<?php if (isset($_POST['add'])) {
    $this->load->view('secure/admin/add/add-destination');
} else {
    ?>
    <form action="<?php echo base_url()?>index.php/admin/view/destination" method="post">
        <input type="submit" class="btn btn-success" name="add" value="Додади">
    </form>
<?php } ?>

<table  class="table table-striped">
    <tr>
        <th>Дестинација</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    $destinations = $this->Destination->get_all();
    foreach ($destinations as $destination) {
        ?>
        <tr>
            <td><?php echo $destination['name'] ?></td>
            <form action="<?php echo base_url();?>index.php/admin/view/admin-companies" method="post">
                <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
            </form>
            <form action="<?php echo base_url()."index.php/admin/delete/destination/".$destination['id'] ?>"method="post">
                <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
            </form>
        </tr>
    <?php } ?>
</table>