<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#bus');
    });
</script>
<!-- Activate the tab END-->

<h2>Автобуси</h2>
<?php if (isset($_POST['add-new'])) {
    $this->load->view('secure/admin/add/add-bus');
} else {
    ?>
    <form action="<?php echo base_url()?>index.php/admin/view/bus" method="post">
        <input type="submit" class="btn btn-success" name="add-new" value="Додади">
    </form>
<?php } ?>

<table class="table table-striped">
    <tr>
        <th>Број на автобус</th>
        <th>Име на компанија</th>
        <th></th>
        <th></th>
    </tr>
    <?php
    $buses = $this->Bus->get_all();
    foreach ($buses as $bus) {
        ?>
        <tr>
            <td><?php echo $bus['bus_number']?></td>
            <td><?php echo $this->Company->get_name($bus['company_id'])?></td>
            <form action="<?php echo base_url();?>index.php/admin/view/admin-companies" method="post">
                <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
            </form>
            <form action="<?php echo base_url()."index.php/admin/delete/bus/".$bus['id'] ?>"method="post">
                <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
            </form>
        </tr>
    <?php } ?>
</table>
