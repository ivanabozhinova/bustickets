<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#userslist');
    });
</script>
<!-- Activate the tab END-->

<h2>Корисници:</h2>
<table class="table table-striped tablesorter">
    <thead>
    <tr>
        <th><h4>Име</h4></th>
        <th><h4>Презиме</h4></th>
        <th><h4>E-mail</h4></th>
        <th><h4>Тип на корисник</h4></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $users = $this->User->get_all();
    foreach($users as $user) {

        ?>
        <td><?php echo $user['first_name'] ?></td>
        <td><?php echo $user['last_name'] ?></td>
        <td><?php echo $user['email'] ?></td>
        <td><?php echo $this->User_type->get_type($user['user_type']) ?></td>
        <form action="<?php echo base_url();?>index.php/admin/view/user" method="post">
            <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
        </form>
        <form action="<?php echo base_url()."index.php/admin/delete/user/".$user['id'] ?>"method="post">
            <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
        </form>

        </tr>
    <?php } ?>
    </tbody>
</table>