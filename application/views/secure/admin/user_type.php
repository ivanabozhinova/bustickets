<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#user_type');
    });
</script>
<!-- Activate the tab END-->

<h2>Типови на корисници</h2>
<?php if (isset($_POST['add'])) {
    $this->load->view('secure/admin/add/add-user-type');
} else {
    ?>
    <form action="<?php echo base_url()?>index.php/admin/view/user_type" method="post">
        <input type="submit" class="btn btn-success" name="add" value="Додади">
    </form>
<?php } ?>
<table class="table table-striped tablesorter">
    <thead>
    <tr>
        <th><h4>Тип на корисник</h4></th>
        <th></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php
    $types = $this->User_type->get_all();
    foreach($types as $type) {

        ?>
        <td><?php echo $type['type'] ?></td>
        <form action="<?php echo base_url();?>index.php/admin/view/admin-companies" method="post">
            <td><button type="submit" class="btn btn-default" value="Измени" name="izmeni"><i class="icon-large icon-edit"></i>   Измени</td>
        </form>
        <form action="<?php echo base_url()."index.php/admin/delete/user_type/".$type['id'] ?>"method="post">
            <td><button type="submit" class="btn btn-default" value='Избриши' name="izbrisi"><i class="icon-large icon-trash"></i>   Избриши</td>
        </form>
        </tr>
    <?php } ?>
    </tbody>
</table>
