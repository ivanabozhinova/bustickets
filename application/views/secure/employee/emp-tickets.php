
<?php //print_r($_SERVER);
 ?>

<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#employeeReservations');
    });
</script>
<!-- Activate the tab END-->

<h2>Билети:</h2>

<hr class="prettyline">
<?php
$tickets_per_user= $this->Ticket->get_all();
// var_dump($all);
?>

<div class="row-fluid">


    <div class="panel-body">

        <table class="table table-striped tablesorter">
            <thead>
            <tr>
                <th><h4>Корисник</h4></th>
                <th><h4>Тргнува </h4></th>
                <th><h4>Пристига</h4></th>
                <th><h4>Време на поаѓање</h4></th>
                <th><h4>Цена<br></h4></th>
                <th><h4>Компанија</h4></th>
                <th><h4>Датум</h4></th>
                <th><h4>Број на седиште</h4></th>
                <th><h4>Датум на купување</h4></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $count = 0;
            foreach($tickets_per_user as $ticket) {

                $line_on_date_data = $this->Line_on_date->get_entry($ticket['line_on_date_id']);
                $line = $this->Line->get_entry_data($line_on_date_data[0]->line_id);
                ?>
                <td><?php echo $this->User->get_user_name($ticket['user_id'])?></td>
                <td><?php echo $line['start_destination_name'] ?></td>
                <td><?php echo $line['end_destination_name'] ?></td>
                <td><?php echo $line['start_time'] ?></td>
                <td><?php echo $line['price'] ?></td>
                <td><?php echo $line['company_name'] ?></td>
                <td><?php echo $line_on_date_data[0]->date ?></td>
                <td><?php echo $ticket['seat_number'] ?></td>
                <?php if (!isset($ticket['date_purchased'])) {
                    echo "<td>Билетот се' уште не е купен.</td>"; ?>

                    <form action="<?php echo base_url()."index.php/employee/mark_purchased/".$ticket['id'];?>" method="post">
                        <td><input type="submit" class="btn btn-warning" value="Означи како купен" name="izbrisi"></td>
                    </form>
                    <?php
                }
                else {
                    ?>
                    <td><?php echo $ticket['date_purchased'] ?></td>
                    <td></td>

                <?php }
                $count++?>
                </tr>
            <?php } ?>
            </tbody>
        </table>

        <ul class="pager">
            <li><a href="#">Previous</a></li>
            <li><a href="#">Next</a></li>
        </ul>
    </div>
</div>