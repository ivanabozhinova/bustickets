<!--Registration BEGIN -->
<div class="row">


    <hr class="prettyline">
    <br>
    <center>
        <button id="signInBtn" class="btn btn-primary btn-lg" href="#signup" data-toggle="modal" data-target=".bs-modal-sm">Онлајн плаќање</button>
    </center>
    <br>
    <hr class="prettyline">



    <!-- Modal -->
    <div class="modal fade bs-modal-sm" id="myModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <br>
                <div class="bs-example bs-example-tabs">
                    <ul id="myTab" class="nav nav-tabs">
                        <li class="active"><a href="#payment" data-toggle="tab">Онлајн плаќање</a></li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div id="myTabContent" class="tab-content">
                        <div class="tab-pane fade active in" id="payment">
                            <form class="form-horizontal">
                                <fieldset>
                                    <!-- Sign In Form -->
                                    <!-- Text input-->
                                    <div class="control-group">
                                        <label class="control-label" for="userAccount">Број на сметка:</label>
                                        <div class="controls">
                                            <input required="" id="userAccount" name="userAccount" type="number"  placeholder="1111 1111 2321 9999"  required="">
                                        </div>
                                    </div>

                                    <!-- Password input-->
                                    <div class="control-group">
                                        <label class="control-label" for="passwordinput">Лозинка:</label>
                                        <div class="controls">
                                            <input required="" id="passwordinput" name="passwordinput" class="form-control" type="password" placeholder="********" class="input-medium">
                                        </div>
                                    </div>

                                    <!-- Button -->
                                    <div class="control-group">
                                        <label class="control-label" for="payment"></label>
                                        <div class="controls">
                                            <button id="payment" name="payment" class="btn btn-success">Уплати</button>
                                        </div>
                                    </div>
                                </fieldset>
                            </form>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Затвори</button>
                    </center>
                </div>
            </div>
        </div>
    </div>

</div>
<!--Registration END -->




