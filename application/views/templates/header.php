<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Le styles -->
    <link href="<?php echo base_url('assets/css/jquery-ui-1.10.1.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/theme.default.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet" media="all">
    <link id="switch_style" href="<?php echo base_url('assets/css/bootstrap.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-responsive.min.css') ?>" rel="stylesheet" media="all">
    <link href="<?php echo base_url('assets/css/bootstrap-responsive.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-theme.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap-theme.min.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/font-awesome.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/custom.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/docs.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/js/google-code-prettify/prettify.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/control.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/control1.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/css/lugo.datepicker.css') ?>" rel="stylesheet">



    <!--


       <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        -->
    <script src="<?php echo base_url('assets/js/jquery.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap.js')?>"></script>
    <!--<script src="<?php //echo base_url('assets/js/bootstrap.min.js')?>"></script> -->
    <script src="<?php echo base_url('assets/js/bootstrap-transition.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-alert.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-scrollspy.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-tab.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-dropdown.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-tooltip.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-popover.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-button.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-collapse.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-carousel.js')?>"></script>
    <script src="<?php echo base_url('assets/js/bootstrap-typeahead.js')?>"></script>
    <script src="<?php echo base_url('assets/js/application.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery.tablesorter.min.js')?>"></script>
    <script src="<?php echo base_url('assets/js/jquery-ui-1.10.1.min.js')?>"></script>



    <script>
        $(document).ready(function() {
            $(".span4 img").hover(function(){
                $(this).animate({height:180, width:300,top: '+=21', left: '+=22' },100);
            },function(){
                $(this).animate({height:180, width:280, top: '+=21', left: '+=22' },100);
            });
        });
    </script>

    <script>
        $(function() {
            $( "#date-picker-input-1" ).datepicker({
                inline: true,
                showOtherMonths: true
            })
                .datepicker('widget').wrap('<div class="ll-skin-lugo"/>');
        });
    </script>


    <!-- Activate the tab BEGIN-->
    <script type="text/javascript">
        function activateTab(tabId) {
            if($("ul.nav li").hasClass('active'))$("ul.nav li").removeClass('active');
            if(!$(tabId).hasClass('active'))$(tabId).addClass('active');
        }
    </script>
    <!-- Activate the tab END-->


    <style>
        #map_canvas {
            width: 360px;
            height: 240px;
        }
    </style>
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>
        function initialize() {
            var map_canvas = document.getElementById('map_canvas');
            var map_options = {
                center: new google.maps.LatLng(41.99051,21.44566),
                zoom: 17,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(map_canvas, map_options);
            // Creating a marker and positioning it on the map
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(41.99051,21.44566),
                map: map});
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
    <script>
        $(document).ready(function()
            {
                $(".tablesorter").tablesorter();
            }
        );
    </script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#signInBtn").trigger('click');
            $("#signInBtn").hide();
        });
    </script>

</head>
<body>


<?php // print_r($_SERVER); 
?>




<div class="container">

    <div class="row-fluid">
        <div class="span9"><br/><h1><img src="<?php echo base_url('assets/img/logo.png')?>"  alt="logo" height="30" width="30">  Автобуска станица</h1></div>
        <div class="span3"><br/>
            <div class="pull-right">
                <a href="#"  original-title="facebook"><img src="<?php echo base_url('assets/icon/soc1.png')?>"  alt="facebook"></a>
                <a href="#"  original-title="Delicious"><img src="<?php echo base_url('assets/icon/soc2.png')?>"  alt="Delicious"></a>
                <a href="#"  original-title="myspace"><img src="<?php echo base_url('assets/icon/soc3.png')?>" alt="myspace"></a><br/><br/>
            </div>
        </div>
    </div>

    <div class="navbar">
        <div class="navbar-inner">
            <div class="container">
                <a data-target=".nav-collapse" data-toggle="collapse" class="btn btn-navbar">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="nav-collapse">
                    <ul class="nav">
                        <?php
                        //if the loged user is an admin
                        if ($logged_in && $admin) {?>
                       <!--     <li id="ticket"><a href="<?php echo base_url()?>/index.php/admin/view/ticket" class="brand">Билети</a></li> -->
                            <li id="company"><a href="<?php echo base_url()?>index.php/admin/view/company">Компании</a></li>
                            <li id="destination"><a href="<?php echo base_url()?>index.php/admin/view/destination">Дестинации</a></li>
                            <li id="line"><a href="<?php echo base_url()?>index.php/admin/view/line">Линии</a></li>
                            <li id="userslist"><a href="<?php echo base_url()?>index.php/admin/view/user">Корисници</a></li>
                            <li id="user_type"><a href="<?php echo base_url()?>index.php/admin/view/user_type">Типови</a></li>
                            <li id="bus"> <a href="<?php echo base_url()?>index.php/admin/view/bus">Автобуси</a></li>
                        <?php }
                        //if the loged user is an employee
                        else if ($logged_in && $employee) {?>
                            <li id="employeeReservations"><a href="<?php echo base_url()?>index.php/employee/view" class="brand">Резервирани билети</a></li>
                        <?php }
                        //if the logged in user is a customer or no one is logged in
                        else {
                            ?>
                            <li id="timetable"><a href="<?php echo base_url()?>index.php/pages/view/timetable" class="brand">Возен ред</a></li>
                            <li id="home" class="active"><a href="<?php echo base_url()?>index.php/pages/view/home">Почетна</a></li>
                            <li id="contact"><a href="<?php echo base_url()?>index.php/pages/view/contact">Контакт</a></li>
                        <?php }?>
                    </ul>

                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <?php if (!$logged_in) {?>
                            <li id="sign-in-temp"><a href="<?php echo base_url()?>index.php/pages/view/sign-in-temp">Најава</a></li>
                            <li id="sign-up-temp"><a href="<?php echo base_url()?>index.php/pages/view/sign-up-temp">Регистрација</a></li>
                        <?php } else  {?>
                            <li id="userprofile"><a href="<?php echo base_url()?>index.php/pages/view/profile"><img src="<?php echo base_url('assets/img/icon-user.png')?>"  alt="logo" height="20" width="20"><?php echo $user_name?></a></li>
                            <li><a href="<?php echo base_url()?>index.php/pages/logout">Одјави се</a></li>
                        <?php } ?>
                    </ul>
                    <form action="" class="navbar-search pull-right">
                        <input type="text" placeholder="Search" class="search-query span2">
                    </form>

                </div><!-- /.nav-collapse -->

            </div>




        </div><!-- /navbar-inner -->

    </div>
