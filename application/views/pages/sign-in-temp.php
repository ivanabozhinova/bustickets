<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#sign-in-temp');
    });
</script>
<!-- Activate the tab END-->

<!--Registration BEGIN -->


<?php
if ($alert == 1) { ?>
    <div class="alert alert-success">Регистрацијата беше успешна.</div>
<?php }
if ($alert == -1) {  ?>
    <div class="alert alert-danger">Внесовте погрешни податоци за најава.</div>
<?php } ?>


<div class="row" xmlns="http://www.w3.org/1999/html">
    <form action="<?php echo base_url();?>index.php/pages/process" class="form-horizontal" method="post">

        <fieldset>
            <!-- Sign In Form -->
            <!-- Text input-->
            <div class="control-group">
                <label class="control-label" for="email">Е-mail:</label>
                <div class="controls">
                    <input type="email" id="email" name="email" class="form-control"  class="input-medium" required="">
                </div>
            </div>

            <!-- Password input-->
            <div class="control-group">
                <label class="control-label" for="password">Лозинка:</label>
                <div class="controls">
                    <input required="" id="password" name="password" class="form-control" type="password"  class="input-medium">
                </div>
            </div>

            <!-- NOT IMPLEMENTED  -->
            <!-- Multiple Checkboxes (inline)
            <div class="control-group">
                <label class="control-label" for="rememberme"></label>
                <div class="controls">
                    <label class="checkbox inline" for="rememberme-0">
                        <input type="checkbox" name="rememberme" id="rememberme-0" value="Remember me">
                        Запамти ме
                    </label>
                </div>
            </div>
            -->

            <!-- Button -->
            <div class="control-group">
                <label class="control-label" for="signin"></label>
                <div class="controls">
                    <input type="submit" id="signin" name="signin" value="Најави се" class="btn btn-success"/>
                </div>
            </div>

        </fieldset>
    </form>

</div>
<!--Registration END -->




