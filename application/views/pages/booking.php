<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#timetable');
    });
</script>
<!-- Activate the tab END-->


<!--Registration BEGIN -->
<div class="row-fluid">

    <?php
    $last = end($this->uri->segments);
    $ticket = $lines[$last];
  //  var_dump($ticket);
    echo "<br><br>";

    //model call in the view.. sorry
    if (isset($_POST['date'])){
        $line_on_date_id = $this->Line_on_date->get_id($ticket['id'], $_POST['date']);
        $url= base_url()."index.php/pages/make_reservation/".$line_on_date_id;
        redirect($url);
    }



    if (!$logged_in) {?>
        <hr class="prettyline">
        <br>
        <center>
            <h4 class="alert alert-danger"><b>За користење на оваа услуга треба да се најавите</b></h4>
            <br>
        <br>
        <hr class="prettyline">

    <?php }
    else
    { ?>


    <div class="span10">
        <form action="" class="form-horizontal" method="post">
            <table class="table table-condensed table-hover ">
                <thead>
                <tr>

                </tr>
                </thead>
                <tbody>

                <tr>
                    <th>Почетна дестинација: </th>
                    <td><?php echo $ticket['start_destination_name']?></td>
                </tr>

                <tr>
                    <th>Крајна дестинација: </th>
                    <td><?php echo $ticket['end_destination_name']?></td>
                </tr>
                <tr>
                    <th>Цена</th>
                    <td><?php echo $ticket['price']?></td>
                </tr>
                <tr>
                    <th>Компанија</th>
                    <td><?php echo $ticket['company_name']?></td>
                </tr>
                <tr>
                    <th>Датум

                    </th>
                    <td>
                        <article>
                            <div>
                            <!--    <input type="text" id="date-picker-input-1" /> -->
                            </div>
                        </article>
                        <select name="date">
                            <?php
                            $line_dates = $this->Line_on_date->get_all_for_line($ticket['id']);
                            foreach($line_dates as $date) {
                                echo "<option>".$date['date']."</option>";
                            }
                            ?>

                        </select></td>
                </tr>
                <tr>
                    <td> </td>
                    <td> </td>
                </tr>

                <tr>
                    <td> </td>
                    <td><input type="submit" class="btn btn-success"  value="Резервирај"/></td>
                </tr>
                </tbody>
            </table>
        </form>

        <?php }?>
        <!--Registration END -->