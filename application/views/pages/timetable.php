<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {

        activateTab('#timetable');

    });
</script>
<!-- Activate the tab END-->



<div class="row" >

        <div class="well well-lg">
            <h1>Преглед на возни редови</h1><br><br>
            <form action="" class="form-search">
                <label style="width:60px;margin-left:10px;"> Поаѓање:</label> <input type="text" name="search_start" placeholder="Почетна дестинација" class="search-query span2">
                <label style="width:80px;margin-left:10px;"> Дестинација:</label>   <input type="text" name="search_end" placeholder="Крајна дестинација" class="search-query span2">
                <input style="margin-left:10px;margin-right:10px;"type="submit" class="btn btn-info btn-lg" value="Пребарај">
            </form>
        </div>
</div>

<div class="row-fluid">
        <!--Vozen red BEGIN -->
        <!-- Default panel contents -->
        <div class="panel-body">
            <table class="table tablesorter table-striped ">
                <thead>
                <tr>
                    <th><h4>Почетна дестинација</h4></th>
                    <th><h4>Крајна дестинација</h4></th>
                    <th><h4>Време на поаѓање</h4></th>
                    <th><h4>Цена на билет /ден.</h4></th>
                    <th><h4>Компанија</h4></th>
                </tr>
                </thead>
                <tbody>
                <?php
                $count = 0;
                foreach($lines as $line) {?>
                <tr>
                    <td><?php echo $line['start_destination_name'] ?></td>
                    <td><?php echo $line['end_destination_name'] ?></td>
                    <td><?php echo $line['start_time'] ?></td>
                    <td><?php echo $line['price'] ?></td>
                    <td><?php echo $line['company_name'] ?></td>
                    <?php if ($logged_in) {?>
                    <td><a href="<?php echo base_url()?>index.php/pages/view/booking/<?php echo $count?>" class="btn btn-default" />Резервирај</a></td>
                    <?php } $count++?>
                </tr>
                <?php } ?>
                </tbody>
            </table>
            <ul class="pager">
                <li><a href="#">Previous</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </div>

        <!-- Table -->
        <!--Vozen red END -->

