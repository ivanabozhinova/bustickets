<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#userprofile');

        //temporary => online payment has to be done first
        $('.btn-warning').click(function () {
            $(this).removeClass('btn-warning').addClass('btn-success');
            //TODO:Change this
            window.location.replace("<?php echo base_url()?>index.php/payment/view/");
            $(this).text('Купен');

        });
    });

</script>
<!-- Activate the tab END-->


<div class="row-fluid">
    <?php
    if (!$logged_in) {?>
        <hr class="prettyline">
        <br>
        <center>
            <h4 class="alert alert-danger"><b>За користење на оваа услуга треба да се најавите</b></h4>
        </center>
        <br>
        <br>
        <hr class="prettyline">

    <?php }
    else
    {


    if ($alert == 1) { ?>
        <div class="alert alert-success">Билетот е успешно резервиран.</div>
    <?php } else if ($alert == -1) {  ?>
        <div class="alert alert-danger">Се случи грешка при резервацијата.</div>
    <?php } ?>


    <h1><?php echo $user_name ?></h1>
    <hr class="prettyline">
    <h3>Резервирани билети:</h3>
    <?php
    $tickets_per_user= $this->Ticket->get_all_for_user($user_id);
    if (isset($seat_number)) {
        if ($seat_number!= -1){
            if ($seat_number==false){
                ?><div class="alert alert-danger">Нема слободни седишта за избраното патување</div><?php
            }
            else {
                ?><div class="alert alert-success">Билетот е успешно резервиран. Поверете ја вашата резервација <a href="<?php echo base_url()?>index.php/pages/view/profile">овде</a>.</div><?php
            }
        }
    }
    ?>
    <div class="row-fluid">
        <div class="panel-body">
            <table class="table table-striped tablesorter">
                <thead>
                <tr>
                    <th><h4>Тргнува </h4></th>
                    <th><h4>Пристига</h4></th>
                    <th><h4>Време на поаѓање</h4></th>
                    <th><h4>Цена<br></h4></th>
                    <th><h4>Компанија</h4></th>
                    <th><h4>Датум</h4></th>
                    <th><h4>Број на седиште</h4></th>
                    <th><h4>Датум на купување</h4></th>


                </tr>
                </thead>
                <tbody>
                <?php
                $count = 0;
                $tickets_per_user = array_reverse($tickets_per_user);
                foreach($tickets_per_user as $ticket) {
                    $line_on_date_data = $this->Line_on_date->get_entry($ticket->line_on_date_id);
                    $line = $this->Line->get_entry_data($line_on_date_data[0]->line_id);
                    ?>
                    <td><?php echo $line['start_destination_name'] ?></td>
                    <td><?php echo $line['end_destination_name'] ?></td>
                    <td><?php echo $line['start_time'] ?></td>
                    <td><?php echo $line['price'] ?></td>
                    <td><?php echo $line['company_name'] ?></td>
                    <td><?php echo $line_on_date_data[0]->date ?></td>
                    <td><?php echo $ticket->seat_number ?></td>
                    <?php if (!isset($ticket->date_purchased)) {
                        echo "<td>Билетот се' уште не е купен.</td>"; ?>
                        <form action="<?php echo base_url()."index.php/payment/mark_purchased/".$ticket->id ?>" method="post">
                            <td><input type="submit" class="btn btn-warning" value="Купи" name="izbrisi"></td>
                        </form>
                    <?php   }
                    else {
                        echo "<td>".$ticket->date_purchased."</td>";
                        echo "<td></td>";
                    }
                    $count++;
                    ?>

                    </tr>
                <?php } ?>
                </tbody>
            </table>

            <ul class="pager">
                <li><a href="#">Previous</a></li>
                <li><a href="#">Next</a></li>
            </ul>
        </div>
        <?php } ?>

