<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#home');
    });
</script>
<!-- Activate the tab END-->



<div class="row-fluid">
    <div class="span7">

        <div class="panel panel-default">
            <!-- Default panel contents -->
            <div class="panel-heading"><h3>Моментални линии</h3></div>
            <div class="panel-body">
                <table class="table table-striped tablesorter">
                    <thead>
                    <tr>
                        <th><h4>Од</h4></th>
                        <th><h4>До</h4></th>
                        <th><h4>Време на поаѓање</h4></th>
                        <th><h4>Компанија</h4></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $count = 0;
                    foreach($lines as $line) {
                        if ($count++>4) break;
                    ?>
                        <tr>
                            <td><?php echo $line['start_destination_name'] ?></td>
                            <td><?php echo $line['end_destination_name'] ?></td>
                            <td><?php echo $line['start_time'] ?></td>
                            <td><?php echo $line['company_name'] ?></td>

                        </tr>
                    <?php } ?>
                    </tbody>
                </table>

            </div>

            <!-- Table -->

        </div>

    </div>
    <div class="span5">
        <div id="map_canvas"></div><div id="map_canvas"></div>
    </div>
</div>
