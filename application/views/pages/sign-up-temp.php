<!-- Activate the tab BEGIN-->
<script type="text/javascript">
    $(document).ready(function() {
        activateTab('#sign-up-temp');
    });
</script>
<!-- Activate the tab END-->


<?php

if ($alert == -1) {  ?>
    <div class="alert alert-danger">Веќе постои корисник со таа емаил адреса.</div>
<?php } ?>


<!--Registration BEGIN -->
<div class="row" xmlns="http://www.w3.org/1999/html">

    <!-- Modal -->
    <div >

        <form action="<?php echo base_url();?>index.php/pages/create_user" class="form-horizontal" method="post">
            <fieldset>
                <!-- Sign Up Form -->
                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="email">Email:</label>
                    <div class="controls">
                        <input id="email" name="email" class="form-control" type="email"  class="input-large" required="">
                    </div>
                </div>

                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="first_name">Име:</label>
                    <div class="controls">
                        <input id="first_name" name="first_name" class="form-control" type="text"  class="input-large" required="">
                    </div>
                </div>

                <div class="control-group">
                    <label class="control-label" for="last_name">Презиме:</label>
                    <div class="controls">
                        <input id="last_name" name="last_name" class="form-control" type="text"  class="input-large" required="">
                    </div>
                </div>


                <!-- Password input-->
                <div class="control-group">
                    <label class="control-label" for="password">Лозинка:</label>
                    <div class="controls">
                        <input id="password" name="password" class="form-control" type="password"  class="input-large" required="">
                    </div>
                </div>

                <!-- Text input-->
                <div class="control-group">
                    <label class="control-label" for="passwordconf">Внеси ја повторно лозинката:</label>
                    <div class="controls">
                        <input id="passwordconf" name="passwordconf" class="form-control" type="password" class="input-large" required="" oninput="check(this)">
                        <script language='javascript' type='text/javascript'>
                            function check(input) {
                                if (input.value != document.getElementById('password').value) {
                                    input.setCustomValidity('Passwords don\'t match.');
                                } else {
                                    // input is valid -- reset the error message
                                    input.setCustomValidity('');
                                }
                            }
                        </script>
                    </div>
                </div>

                <!-- Button -->
                <div class="control-group">
                    <label class="control-label" for="confirmsignup"></label>
                    <div class="controls">
                        <input type="submit" id="confirmsignup" name="confirmsignup" value="Регистрирај се" class="btn btn-success"/>
                    </div>
                </div>
            </fieldset>
        </form>
    </div>
</div>

<!--Registration END -->




