<?php

class Add_data extends CI_Controller {

    //dodava po (predefinirano) 10 avtobusi (so random broevi) za sekoja kompanija
    public function add_buses($number = 10)
    {
        $companies = $this->Company->get_all();
        echo "Adds buses for each company (with random bus numbers)";
        echo "<br>";
        echo "<br>";

        foreach ($companies as $company){
            $id = $company['id'];
            $_POST['company_name'] = $company['name'];
            echo "<br><br>Company: ".$company['name']."<br>";
            for ($i=0; $i<$number; $i++){
                $_POST['bus_number'] = rand(10, 500);
                print_r($_POST);
                echo "<br>";
                $this->Bus->insert_entry();
            }
        }
    }

    //dodava po edna linija za sekoj avtobus
    public function add_line()
    {
        //get all destinations
        $destinations = $this->Destination->get_all();
        //put all the destination ids in an array (in order to choose randomly)
        $destination_ids = array();
        foreach ($destinations as $bus){
            $id = $bus['id'];
            array_push($destination_ids, $id);
        }

        //get all the buses
        $buses = $this->Bus->get_all();

        //generate array for the prices
        $prices = array();
        for ($i=150; $i<700; $i+=50){
            array_push($prices, $i);
        }

        foreach ($buses as $bus){
            //get each bus id
            $bus_id = $bus['id'];

            //generate the start destination
            $start_id = $destination_ids[array_rand($destination_ids)];

            //generate the end destinaton
            $end_id = $destination_ids[array_rand($destination_ids)];
            while($start_id==$end_id)
                $end_id = $destination_ids[array_rand($destination_ids)];

            //generate the price
            $price = $prices[array_rand($prices)];

            $time = rand(0,24).":00:00";

            echo "<br>Bus ".$id.": From: ".$start_id." To: ".$end_id."<br>Price: ".$price." At: ".$time."<br><br>";
            $this->Line->insert_entry_fill_database($start_id, $end_id, $time, $bus_id, $price);
        }
    }

    public function add_line_on_date($days = 50)
    {
        //get all lines
        $lines = $this->Line->get_all();

        foreach ($lines as $line){
            $line_id = $line['id'];
            $date = date('Y-m-d');
           // echo "<br><br><br>INITIAL: ".$date."<br>";
            for ($i=0; $i<$days; $i++){
                //temporary
                $this->Line_on_date->insert_entry($line_id, $date);
                $date = strtotime($date) + (60 * 60 * 24); //increase date by 1 day
                $date = date('Y-m-d', $date);
                echo  "<br>Line number: ".$line['id']." on ".$date;
            }
        }
    }



}