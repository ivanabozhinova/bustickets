<?php

class Employee extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['SSL_CLIENT_S_DN_CN'])) {
            if (strcmp($_SERVER['SSL_CLIENT_S_DN_CN'], 'gashteovski.andrej@students.finki.ukim.mk') != 0 && strcmp($_SERVER['SSL_CLIENT_S_DN_CN'], 'ibozhinova') != 0) {
                echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
                exit();
            }
        } else {
            echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
            exit();
        }
    }


    public function view($page = 'emp-tickets')
    {
        if (!file_exists('application/views/secure/employee/' . $page . '.php'))
            show_404();

        if (preg_match('/([A-Za-z0-9\-\_]+)/', $page)) {
            // string contains only english letters
            $data['title'] = ucfirst($page); // Capitalize the first letter

        } else
            show_404();

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logged_in'] = $this->User->check_validated();
        $data['user_id'] = $this->session->userdata('id');
        $data['admin'] = $this->User->admin_check();
        $data['employee'] = $this->User->employee_check();
        $data['user_name'] = $this->session->userdata('first_name') . " " . $this->session->userdata('last_name');
        $data['lines'] = $this->Line->get_table_data();
        $data['admin'] = $this->User->admin_check();
        $data['message'] = "Немате доволно привилегии за пристап до содржината на страната!";
        $this->load->view('templates/header', $data);
        if ($this->User->employee_check())
            $this->load->view('secure/employee/' . $page, $data);
        else
            $this->load->view('pages/permission-denied', $data);
        $this->load->view('templates/footer', $data);
    }

    public function mark_purchased($id)
    {
        $this->Ticket->mark_purchased($id);
        redirect('employee/view/');
    }

}