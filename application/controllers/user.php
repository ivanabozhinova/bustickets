<?php

class Pages extends CI_Controller {
    //$message - to show different messages in permission-denied.php
    public function view($page = 'home',$message="" ,$alert=0)
    {
        if ( ! file_exists('application/views/secure/user'.$page.'.php'))
        {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logged_in'] = $this->check_validated();
        $data['admin'] = $this->User->admin_check();
        $data['employee'] = $this->User->employee_check();
        $data['user_id'] = $this->session->userdata('id');
        $data['user_name'] = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
        $data['lines']=$this->Line->get_table_data();
        $data['alert']=$alert;
        $data['message']=$message;
        $this->load->view('templates/header', $data);
        $this->load->view('pages/secure/user'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    private function check_validated(){
        return $this->session->userdata('validated');
    }

    public function make_reservation($line_on_date_id){
        $user_id = $this->session->userdata('id');
        $seat_number = $this->Line_on_date->update_seats($line_on_date_id);
        if ($seat_number == false)
            $this->view('profile', -1);
        $this->Ticket->insert_entry($line_on_date_id, $user_id, $seat_number);
        $this->view('profile', 1);
     //   redirect('pages/view/profile');
    }

    public function process(){
        // Validate the user can login
        $result = $this->User->validate();
        // Now we verify the result
        if(! $result){
            // If user did not validate, then show them login page again
          //  redirect('pages/view/sign-in-temp');
            $this->view('sign-in-temp', -1);
        }else{
            // If user did validate,
            // Send them to members area
            if($this->User->admin_check())
            redirect('admin/view');
            else if($this->User->employee_check())
            redirect('employee/view');
            else redirect('pages/view');
        }
    }

    public function logout(){
        $this->session->sess_destroy();
        redirect('pages/view');
    }

    public function create_user(){
       //if the user is inserted into the database returns true ; else false ;
       if($this->User->insert_entry())
        $this->view('sign-in-temp', 1);
       //insert_entry returns false if the email is already taken
        else{
                $data='Веќе постои корисник со таа емаил адреса !';
                $this->view('permission-denied',$data);
        }
    }
}