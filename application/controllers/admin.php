<?php

class Admin extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['SSL_CLIENT_S_DN_CN'])) {
            if (strcmp($_SERVER['SSL_CLIENT_S_DN_CN'], 'gashteovski.andrej@students.finki.ukim.mk') != 0) {
                echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
                exit();
            }
        } else {
            echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
            exit();
        }
    }


    public function view($page = 'company')
    {
        if (!file_exists('application/views/secure/admin/' . $page . '.php'))
            show_404();

        // string contains only english letters
        if (!preg_match('/([A-Za-z0-9\-\_]+)/', $page))
            show_404();

        $data['title'] = ucfirst($page); // Capitalize the first letter
        $data['logged_in'] = $this->User->check_validated();
        $data['user_id'] = $this->session->userdata('id');
        $data['admin'] = $this->User->admin_check();
        $data['employee'] = $this->User->employee_check();
        $data['user_name'] = $this->session->userdata('first_name') . " " . $this->session->userdata('last_name');
        $data['lines'] = $this->Line->get_table_data();
        $data['admin'] = $this->User->admin_check();
        $data['message'] = "Немате доволно привилегии за пристап до содржината на страната!";
        $this->load->view('templates/header', $data);
        if ($this->User->admin_check())
            $this->load->view('secure/admin/' . $page, $data);
        else
            $this->load->view('pages/permission-denied', $data);
        $this->load->view('templates/footer', $data);
    }

    public function add($table)
    {
        $model = ucfirst($table);
        $this->$model->insert_entry();
        redirect('admin/view/' . $table);
    }

    public function edit($table, $id)
    {
        $model = ucfirst($table);
        $this->$model->edit_entry($id);
        redirect('admin/view/' . $table);
    }

    public function delete($table, $id)
    {
        $model = ucfirst($table);
        $this->$model->delete_entry($id);
        redirect('admin/view/' . $table);
    }


}