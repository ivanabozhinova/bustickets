<?php

class Payment extends CI_Controller {

   function __construct()
    {
        parent::__construct();
        if (isset($_SERVER['SSL_CLIENT_S_DN_CN']) || isset($_SERVER['SSL_CLIENT_I_DN_CN'])) {
            if (isset($_SERVER['SSL_CLIENT_S_DN_CN']) &&
                strcmp($_SERVER['SSL_CLIENT_S_DN_CN'], 'gashteovski.andrej@students.finki.ukim.mk') != 0 &&
                strcmp($_SERVER['SSL_CLIENT_I_DN_CN'], 'gashteovski.andrej@students.finki.ukim.mk') != 0) {
                echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
                exit();
            }
        } else {
            echo 'Nema sertifikat ili korisnikot nema permisii da pristapi ovde';
            exit();
        }

    }

    public function view($page = 'online-payment')
    {	
	if ( ! file_exists('application/views/secure/user/'.$page.'.php'))
            show_404();

	if (preg_match('/([A-Za-z0-9\-\_]+)/', $page)) 
	{
  		// string contains only english letters
		$data['title'] = ucfirst($page); // Capitalize the first letter
		 
	}else
		show_404();

        $data['logged_in'] = $this->check_validated();
        $data['admin'] = $this->User->admin_check();
        $data['employee'] = $this->User->employee_check();
        $data['user_id'] = $this->session->userdata('id');
        $data['user_name'] = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
        $data['lines']=$this->Line->get_table_data();
        $this->load->view('templates/header', $data);
        $this->load->view('secure/user/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function success($page = 'payment-success')
    {
        if ( ! file_exists('application/views/secure/user/'.$page.'.php'))
            show_404();
	if (preg_match('/([A-Za-z0-9\-\_]+)/', $page)) 
	{
  		// string contains only english letters
		$data['title'] = ucfirst($page); // Capitalize the first letter
		 
	}else
		show_404();

        $data['logged_in'] = $this->check_validated();
        $data['admin'] = $this->User->admin_check();
        $data['employee'] = $this->User->employee_check();
        $data['user_id'] = $this->session->userdata('id');
        $data['user_name'] = $this->session->userdata('first_name')." ".$this->session->userdata('last_name');
        $data['lines']=$this->Line->get_table_data();
        $this->load->view('templates/header', $data);
        $this->load->view('secure/user/'.$page, $data);
        $this->load->view('templates/footer', $data);
    }

    public function mark_purchased($id)
    {
        $this->Ticket->mark_purchased($id);
        redirect('payment/view/');
    }


    private function check_validated(){
        return $this->session->userdata('validated');
    }

}