<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:10
 */

class Line extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_all()
    {
        $query = $this->db->get('line');
        return $query->result_array();
    }

    function get_table_data()
    {
        $data=$this->get_all();
        $result = array();
        foreach ($data as $row){
            $row['start_destination_name'] = $this->Destination->get_name($row['start_destination_id']);
            $row['end_destination_name'] = $this->Destination->get_name($row['end_destination_id']);
            $row['company_name'] = $this->Company->get_name($this->Bus->get_company_id($row['bus_id']));
            $row['bus_number'] = $this->Bus->get_bus_number($row['bus_id']);

            array_push($result, $row);
        }
        return $result;
    }

//so get treba da se zemat
    function search_data($start, $end)
    {
        $data = array(
            'start_destination_id' => $this->Destination->get_id($start),
            'end_destination_id' => $this->Destination->get_id($end)
        );
        $query = $this->db->get_where('line', $data);
//        $result= $query->result_array();
//
//        $result = array();
//        foreach ($data as $row){
//            $row['start_destination_name'] = $this->Destination->get_name($row['start_destination_id']);
//            $row['end_destination_name'] = $this->Destination->get_name($row['end_destination_id']);
//            $row['company_name'] = $this->Company->get_name($this->Bus->get_company_id($row['bus_id']));
//            $row['bus_number'] = $this->Bus->get_bus_number($row['bus_id']);
//
//            array_push($result, $row);
//        }
//        return $result;
    }

    function get_entry($id)
    {
        $query = $this->db->get_where('line', array('id' => $id));
        return $query->result_array();
    }

    function get_entry_data($id)
    {
        $data=$this->get_entry($id);
        //podobro $row = $data[0];
        foreach ($data as $row){
            $row['start_destination_name'] = $this->Destination->get_name($row['start_destination_id']);
            $row['end_destination_name'] = $this->Destination->get_name($row['end_destination_id']);
            $row['company_name'] = $this->Company->get_name($this->Bus->get_company_id($row['bus_id']));
            $row['bus_number'] = $this->Bus->get_bus_number($row['bus_id']);
            return $row;
        }
    }

    function edit_entry($id)
    {
        $data = array(
            'start_destination_id' => $this->Destination->get_id($_POST['start_destination_name']),
            'end_destination_id' => $this->Destination->get_id($_POST['end_destination_name']),
            'start_time' => $_POST['start_time'],
            'bus_id' => $this->Bus->get_bus_id($_POST['bus_number'], $_POST['company_name']),
            'price' => $_POST['price']
        );
        $this->db->where('id', $id);
        $this->db->update('bus', $data);
    }

    function insert_entry()
    {
        $this->start_destination_id = $this->Destination->get_id($_POST['start_destination_name']);
        $this->end_destination_id = $this->Destination->get_id($_POST['end_destination_name']);
        $this->start_time = $_POST['start_time'];

        $bus =$_POST['bus'];
        $parts = explode(": ", $bus);
        $this->bus_id = $this->Bus->get_bus_id(trim($parts[0]), trim($parts[1]));

        $this->price = $_POST['price'];
        $this->db->insert('line', $this);
    }

    function delete_entry($id)
    {
        $this->db->delete('line', array('id' => $id));
    }

    function insert_entry_fill_database($start_id, $end_id, $time, $bus_id, $price)
    {
        $this->start_destination_id = $start_id;
        $this->end_destination_id = $end_id;
        $this->start_time = $time;
        $this->bus_id = $bus_id;
        $this->price = $price;
        $this->db->insert('line', $this);
    }



}