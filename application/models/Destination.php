<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:14
 */

class Destination extends CI_Model{


    function get_name($id)
    {
        $query = $this->db->get_where('destination', array('id' => $id));
       foreach ($query->result() as $row)
           return $row->name;
    }


    function get_id($name)
    {
        $query = $this->db->get_where('destination', array('name' => $name));
        foreach ($query->result() as $row)
            return $row->id;
    }


    function get_all()
    {
        $query = $this->db->get('destination');
        return $query->result_array();
    }

    function edit_entry($id)
    {
        $data = array(
            'name' => $_POST['name']
        );
        $this->db->where('id', $id);
        $this->db->update('destination', $data);
    }

    function insert_entry()
    {
        $this->name = $_POST['name'];
        $this->db->insert('destination', $this);
    }

    function delete_entry($id)
    {
        $this->db->delete('destination', array('id' => $id));
    }





} 