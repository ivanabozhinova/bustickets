<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:15
 */

class User_type extends CI_Model{

    function get_type($id)
    {
        $query = $this->db->get_where('user_type', array('id' => $id));
        foreach ($query->result() as $row)
            return $row->type;
    }


    function get_all()
    {
        $query = $this->db->get('user_type');
        return $query->result_array();
    }

    function edit_entry($id)
    {
        $data = array(
            'type' => $_POST['type']
        );
        $this->db->where('id', $id);
        $this->db->update('user_type', $data);
    }

    function insert_entry()
    {
        $this->type = $_POST['type'];
        $this->db->insert('user_type', $this);
    }

    function delete_entry($id)
    {
        $this->db->delete('user_type', array('id' => $id));
    }
}