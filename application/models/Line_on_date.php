<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:14
 */

class Line_on_date extends CI_Model{

    function get_id($line_id, $date)
    {
        $conditions['line_id'] =$line_id;
        $conditions['date'] =$date;
        $query = $this->db->get_where('line_on_date',$conditions);
        foreach ($query->result() as $row)
            return $row->id;
    }

    function get_entry($id)
    {
        $query = $this->db->get_where('line_on_date', array('id' => $id));
        return $query->result();
    }

    function update_seats($id)
    {
//	var_dump($this->get_entry($id)
	$query = $this->db->get_where('line_on_date', array('id' => $id));
        foreach ($query->result() as $row){
	 $entry = $row;
	break;
	}

        $seats = $entry->available_seats;
        if ($seats==0)
            return false;
        $seats--;
        $this->db->update('line_on_date', array('available_seats' => $seats), 'id = '.$id);
        return $seats;
    }

    function insert_entry($line_id, $date, $seats=50){
        $this->line_id = $line_id;
        $this->date = $date;
        $this->available_seats = $seats;
        $this->db->insert('line_on_date', $this);
    }

    function get_all_for_line($line_id)
    {
        $query = $this->db->get_where('line_on_date', array('line_id' => $line_id));
        return $query->result_array();
    }

}