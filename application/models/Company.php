<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:14
 */

class Company extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function get_name($id)
    {
        $query = $this->db->get_where('company', array('id' => $id));
        foreach ($query->result() as $row)
            return $row->name;
    }

    function get_id($name)
    {
        $query = $this->db->get_where('company', array('name' => $name));
        foreach ($query->result() as $row)
            return $row->id;
    }

    function get_all()
    {
        $query = $this->db->get('company');
        return $query->result_array();
    }

    function edit_entry($id)
    {
        $data = array(
            'name' => $_POST['name']
        );
        $this->db->where('id', $id);
        $this->db->update('company', $data);
    }

    function insert_entry()
    {
        $this->name = $_POST['name'];
        $this->db->insert('company', $this);
    }

    function delete_entry($id)
    {
        $this->db->delete('company', array('id' => $id));
    }
} 