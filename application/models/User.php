<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:15
 */

class User extends CI_Model{

    function is_email_new(){
        $username = $this->security->xss_clean($this->input->post('email'));
        $this->db->where('email', $username);
        $query = $this->db->get('user');
        if($query->num_rows > 0)
        {
            // If the email is a used, then return false

            return false;
        }
        // If there isn't such email this is a new user
        // so return true.
        return true;

    }
    function insert_entry($user_type=3)
    {
        //email taken, user not created
        if(!$this->is_email_new())
            return false;
        else{


            $this->email  = $_POST['email'];
            $this->first_name = $_POST['first_name'];
            $this->last_name = $_POST['last_name'];


            $this->password = $this->bcrypt->hash_password($_POST['password']);

            $this->user_type = $user_type;
            $this->db->insert('user', $this);
            //user is created
            return true;
        }
    }

    public function validate1(){
        // grab user input
        $username = $this->security->xss_clean($this->input->post('email'));
        $password = $this->bcrypt->hash_password(($this->input->post('password')));

        // Prep the query
        $this->db->where('email', $username);
        $this->db->where('password', $password);

        // Run the query
        $query = $this->db->get('user');
        // Let's check if there are any results

        if($query->num_rows == 1)
        {
            // If there is a user, then create session data
            $row = $query->row();

            $data = array(
                'id' => $row->id,
                'first_name' => $row->first_name,
                'last_name' => $row->last_name,
                'email' => $row->email,
                'user_type' => $row->user_type,
                'validated' => true
            );
        $this->session->set_userdata($data);
        return true;
    }
    // If the previous process did not validate
        // then return false.
        return false;
    }


    public function validate() {
        $email= $this->input->post('email');
        $password =$this->input->post('password');

        $this->db->where('email',$email);

        $result = $this->getUsers($password);

        if (!empty($result)) {
            return true;
        } else {
            return false;
        }
    }
    function getUsers($password) {
        $query = $this->db->get('user');
        if ($query->num_rows() > 0) {
            $row = $query->row();
            if ($this->bcrypt->check_password($password, $row->password)) {
                //We're good

                $data = array(
                    'id' => $row->id,
                    'first_name' => $row->first_name,
                    'last_name' => $row->last_name,
                    'email' => $row->email,
                    'user_type' => $row->user_type,
                    'validated' => true
                );
                $this->session->set_userdata($data);
                return $row;
            } else {
                //Wrong password
                return array();
            }
        } else {
            return array();
        }
    }

    public function check_validated(){
        return $this->session->userdata('validated');
    }

    public function admin_check(){
        $type_id = $this->session->userdata('user_type');
        $type_name = $this->User_type->get_type($type_id);
        return (!strcmp($type_name, 'admin'));
    }


    public function employee_check(){
        $type_id = $this->session->userdata('user_type');
        $type_name = $this->User_type->get_type($type_id);
        return (!strcmp($type_name, 'employee'));
    }

    function get_all()
    {
        $query = $this->db->get('user');
        return $query->result_array();
    }

    function edit_entry($id)
    {
        $data = array(
            'email'  => $_POST['email'],
            'first_name' => $_POST['first_name'],
            'last_name' => $_POST['last_name'],
            'password' => $_POST['password'],
            'user_type' => 2//$user_type -employee
        );
        $this->db->where('id', $id);
        $this->db->update('bus', $data);
    }



    function delete_entry($id)
    {
        $this->db->delete('user', array('id' => $id));
    }

    function get_user_name($id)
    {
        $query = $this->db->get_where('user', array('id' => $id));
        foreach ($query->result() as $row)
            return $row->first_name." ".$row->last_name;
    }


}