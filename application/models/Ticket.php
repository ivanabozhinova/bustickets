<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:15
 */

class Ticket extends CI_Model{

    function insert_entry($line_on_date_id, $user_id, $result)
        //result e od proverkata na slobodni sedista (false ako nema slobodni mesta, inaku brojot na slobodni mesta)
    {
        if ($result!=false){
            $this->line_on_date_id  = $line_on_date_id;
            $this->user_id = $user_id;
            $this->seat_number = 50-$result;
            $this->db->insert('ticket', $this);
        }
    }

    function get_all_for_user($user_id)
    {
        $query = $this->db->get_where('ticket', array('user_id' => $user_id));
        return $query->result();
    }

    function get_all()
    {
        $query = $this->db->get('ticket');
        return $query->result_array();
    }

    function mark_purchased($id)
    {
        $data = array(
            'date_purchased' => date('Y-m-d')
        );
        $this->db->where('id', $id);
        $this->db->update('ticket', $data);
    }



}