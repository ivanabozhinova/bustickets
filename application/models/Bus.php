<?php
/**
 * Created by PhpStorm.
 * User: amc
 * Date: 20.4.14
 * Time: 06:14
 */

class Bus extends CI_Model{

    function __construct()
    {
        parent::__construct();
    }

    function get_company_id($id)
    {
        $query = $this->db->get_where('bus', array('id' => $id));
        foreach ($query->result() as $row)
            return $row->company_id;
    }

    function get_bus_id($bus_number, $company_name)
    {
        $company_id = $this->Company->get_id($company_name);
        $query = $this->db->get_where('bus', array('company_id' => $company_id, 'bus_number' => $bus_number));
        foreach ($query->result() as $row)
            return $row->id;
    }

    function get_buses_by_company($company_name)
    {
        $company_id = $this->Company->get_id($company_name);
        $query = $this->db->get_where('bus', array('company_id' => $company_id));
        return $query->result_array();
    }



    function get_bus_number($id)
    {
        $query = $this->db->get_where('bus', array('id' => $id));
        foreach ($query->result() as $row)
            return $row->bus_number;
    }

    function get_all()
    {
        $query = $this->db->get('bus');
        return $query->result_array();
    }

    function get_all_data()
    {
        $result = array();
        $query = $this->db->get('bus');
        foreach ($query->result_array() as $row)
        {
            $c_name = $this->Company->get_name($row['company_id']);
            $row['print'] = $row[bus_number].": ".$c_name;
            array_push($result, $row);
        }
        return $result;
    }

    function edit_entry($id)
    {
        $data = array(
            'bus_number' => $_POST['bus_number'],
            'company_id' => $this->Company->get_id($_POST['company_name'])
        );
        $this->db->where('id', $id);
        $this->db->update('bus', $data);
    }

    function insert_entry()
    {
        $this->bus_number = $_POST['bus_number'];
        $this->company_id = $this->Company->get_id($_POST['company_name']);
        $this->db->insert('bus', $this);
    }

    function delete_entry($id)
    {
        $this->db->delete('bus', array('id' => $id));
    }

} 